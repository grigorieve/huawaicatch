﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace HuawaiCatch
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver();
            IJavaScriptExecutor javaScript = driver as IJavaScriptExecutor;

            driver.Url = "https://shop.huawei.com/ru/product/huawei-mate-20-pro";
            Thread.Sleep(5000);

            IWebElement e = driver.FindElement(By.CssSelector("#pro-special-price"));

            int price;
            int.TryParse(string.Join("", e.Text.Where(c => char.IsDigit(c))), out price);
            Console.WriteLine(price);

            javaScript.ExecuteScript("window.scrollTo(0, 500)");
            driver.FindElement(By.CssSelector(".temp-buy-btn")).Click();

            Thread.Sleep(5000);

            driver.FindElement(By.CssSelector(".guest-checkout")).Click();

            Thread.Sleep(5000);

            
            driver.FindElement(By.CssSelector(".fancybox-close")).Click();

            //Actions action = new Actions(driver);
            //action.SendKeys(OpenQA.Selenium.Keys.Escape);

            e = driver.FindElement(By.CssSelector(".order-pro-area .tr-price"));
            int.TryParse(string.Join("", e.Text.Where(c => char.IsDigit(c))), out price);
            Console.WriteLine(price);

            Thread.Sleep(5000);
            Console.ReadLine();
        }
    }
}
